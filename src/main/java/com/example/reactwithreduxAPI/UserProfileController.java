package com.example.reactwithreduxAPI;

import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping(value = "/api/user-profiles")
public class UserProfileController {
    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public UserProfile getUser(@PathVariable Long id) {
        if (id == 1) {
            return new UserProfile("Ipsum Dolor",
                    "Male",
                    "Secondary copy lorem ipsum dolor sit amet, consectetur " +
                            "adipiscing elit. Fusce suscipit, tortor sed eleifend, nibh lacus mattis massa," +
                            " id ullamcorper erat libero in est. " +
                            "Donec dapibus dolor sed interdum gravida.");
        } else if(id == 2) {
            return new UserProfile("Ihsbf Bowsa",
                    "Female",
                    "This is the description for Ihsbf Bowsa.");
        } else {
            return null;
        }
    }
}
